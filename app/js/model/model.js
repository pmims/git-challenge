define(['backbone', 'jquery', 'underscore'], function(Backbone, $, _) {
    var model = Backbone.Model.extend({
        initialize: function() {
            console.log("model loaded...");
        },
        //url:"https://api.github.com/search/repositories?q=tetris+language:assembly&sort=stars&order=desc"
        url: "https://api.github.com/search/repositories?q=stars:>500"
    });
    return model;
});
