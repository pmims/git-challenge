define(['backbone','jquery','underscore'], function(Backbone,$,_) {
    return view_two = Backbone.View.extend({
        initialize: function() {
            console.log("view 2 loaded...");
            this.listenTo(this.model, 'change', this.render);
            this.model.fetch();
        },

        template: _.template($("#main-two-template").html()),

        el: "#route",

        render: function() {
            var self = this;
            return self.$el.html(self.template(self.model.toJSON()));
        }
    });
});
