define(['jquery', 'underscore', 'navView'], function($, _ ) {
	var view = NavView.extend({
		initialize: function() {
				this.listenTo(this.model, 'change', this.render);
				this.model.fetch();
		},
		template: _.template($("#credit-template").html()),
	});
	return creditView;
});

3         return navView = Backbone.View.extend({
  4             initialize: function() {
  5                 var self = this;
  6                 self.listenTo(self.model, 'change', self.render);
  7                 self.model.fetch();
  8             },
  9 
 10             el: "#content",
 11 
 12             render: function() {
 13                 var self = this;
 14                 return self.$el.html(self.template(self.model.toJSON()));
 15             }
 16         });
