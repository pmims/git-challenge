define(['backbone','jquery','underscore'], function(Backbone,$,_) {
    return view = Backbone.View.extend({
        initialize: function()  {
            console.log("view loaded...");
            this.listenTo(this.model, 'change', this.render);
            this.model.fetch();
        },

        template: _.template($("#main-template").html()),

        el: "#content",

        render: function() {
            var self = this;
            return self.$el.html(self.template(self.model.toJSON()));
        }
    });
});
