require.config ({
	baseUrl: 'app',
	paths: {
		jquery:     '../node_modules/jquery/dist/jquery',
		underscore: '../node_modules/underscore/underscore',
		backbone:   '../node_modules/backbone/backbone',

        model:      'js/model/model',
        view:       'js/view/view',

        router:     'js/route/route',
	},

	shim: {
		underscore: {
			exports: '_'
		},
		backbone: {
			deps: [
				'underscore',
				'jquery'
			],
			exports: 'Backbone'
		}
	}
});

define(['view', 'model', 'router'], 
    function(view, model, router) {

        new router;
        Backbone.history.start();

        new view ({
            model: new model
        });
    });
